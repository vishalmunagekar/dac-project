import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  msg: string;
  num: number;
  constructor(private service: DataService) { this.num = 0 }

  ngOnInit() { }

  FormValidation(UserData: any) {
    if (UserData.form.value.uname == "" || UserData.form.value.email == "" ||
      UserData.form.value.phone == "" || UserData.form.value.dob == "" ||
      UserData.form.value.pwd == "") {
      this.msg = "All fields are compulsory!!";
    }
    else {
      let resultstate = this.service.SignupData(UserData.form.value);
      resultstate.subscribe((data: any) => {
        if (data.error == null) {
          this.msg = "Successfully Signed Up!!";
          this.num = 1;
        }
        else {
          console.log(data);
          this.msg = "Somthing went Wrong!!";
          this.num = 0;
        }
      });
    }
  }
}
