var express = require('express');
var mysql = require('mysql');
var EditProfileRouter = express();
var crypto = require('crypto-js');

const connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'manager',
  database: 'pizzahot_db'
});

connection.connect();

EditProfileRouter.get("/:uname", function (req, res) {
  let uname = req.params.uname;
  let query = `SELECT * FROM signup WHERE uname='${uname}'`;
  connection.query(query, function (err, result) {
    if (err == null) {
      res.contentType("application/json");
      res.send(JSON.stringify(result));
    }
    else {
      res.send("Something went wrong!");
    }
  });
});

EditProfileRouter.put("/:uname", function (req, res) {
  let uname = req.body.uname;
  let email = req.body.email;
  let phone = req.body.phone;
  let dob = req.body.dob;
  let pwd = crypto.MD5(req.body.pwd);
  let query = `UPDATE signup SET uname='${uname}',
                                 email='${email}',
                                 phone=${phone},
                                 dob='${dob}',
                                 pwd='${pwd}', WHERE uname='${uname}'`;
  connection.query(query, function (err, result) {
    if (err == null) {
      res.contentType("application/json");
      res.send(JSON.stringify(result));
    }
    else {
      res.contentType("application/json");
      res.send(err);
    }
  });
});

module.exports = EditProfileRouter;