import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ContactUsComponent } from './contact-us/contact-us.component';

import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { AuthService } from './auth.service';
import { DataService } from './data.service';
import { AppRoutingModule } from './app-routing.module';
import { ProfileComponent } from './profile/profile.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignUpComponent,
    HomeComponent,
    NotFoundComponent,
    AboutUsComponent,
    ContactUsComponent,
    ProfileComponent,
    EditProfileComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    RouterModule.forRoot([
      {path : "" , component : HomeComponent, canActivate: [AuthService]},
      {path : "Home" , component : HomeComponent, canActivate: [AuthService]},
      {path : "AboutUs" , component : AboutUsComponent},
      {path : "Profile" , component : ProfileComponent },
      {path : "EditProfile/:uname" , component : EditProfileComponent, canActivate: [AuthService]},
      {path : "ContactUs" , component : ContactUsComponent},
      {path : "Login" , component : LoginComponent},
      {path : "SignUp" , component : SignUpComponent},
      {path : "**" , component : NotFoundComponent}
    ]),
    AppRoutingModule
  ],
  providers: [HttpClientModule, DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
