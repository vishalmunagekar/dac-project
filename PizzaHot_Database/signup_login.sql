create database pizzahot_db;

use pizzahot_db;

CREATE TABLE signup (
	uid int auto_increment primary key,
    uname VARCHAR(40) unique key,
    email VARCHAR(50),
    phone BIGINT unique key,
    dob DATE,
    pwd VARCHAR(50)
);

CREATE TABLE login (
    uname VARCHAR(40),
    pwd VARCHAR(50)
);

DELIMITER ;
DROP TRIGGER IF EXISTS trig_login_after_insert;
DELIMITER $$
CREATE TRIGGER trig_login_after_insert
AFTER INSERT ON signup
FOR EACH ROW
BEGIN
INSERT INTO login VALUES(NEW.uname, NEW.pwd);
END;
$$
DELIMITER ;

DELIMITER ;
DROP TRIGGER IF EXISTS trig_login_after_update;
DELIMITER $$
CREATE TRIGGER trig_login_after_update
AFTER UPDATE ON signup
FOR EACH ROW
BEGIN
UPDATE login SET uname=NEW.uname, pwd=NEW.pwd WHERE uname=OLD.uname;
END;
$$
DELIMITER ;


INSERT INTO signup (uname, email, phone, dob, pwd) VALUES('vishalm', 'munagekarvishal@gmail.com', 7843052772, '1995-01-17', 'vishalm2772');
INSERT INTO signup (uname, email, phone, dob, pwd) VALUES('vishalmunagekar', 'vishalmunagekar@gmail.com', 7972671282, '1995-01-17', 'vishalm3535');
INSERT INTO signup (uname, email, phone, dob, pwd) VALUES('ajinkya121', 'ajinkyamane3996@gmail.com', 9960735906, '1995-06-17', 'ajinkya121');
INSERT INTO signup (uname, email, phone, dob, pwd) VALUES('sumitmane', 'ajinkyamane3996@gmail.com', 8668545265, '1995-01-17', 'sumit5051');


UPDATE login SET uname=NEW.uname, pwd=NEW.pwd WHERE uname=NEW.uname;

UPDATE signup SET uname='Vishal007', email='vishalmunagekar@gmail.com', phone=7843052772, dob='1997-01-17', pwd='qwertyuiop' WHERE uname='vishalm';
UPDATE signup SET uname='vishalm', email='vm@gmail.com', phone=7788962021, dob='1997-01-17', pwd='qwertyuiop' WHERE uname='Vishal007';


















