import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../data.service';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {
  UserData;
  msg;
  constructor(public routes: ActivatedRoute,
    public DataService: DataService,
    public router: Router) {
    this.msg = "";
  }

  ngOnInit() {
    let EditParas = this.routes.paramMap;

    EditParas.subscribe((params) => {
      let uname = params.get("uname");
      let StatusOfUserSearched = this.DataService.GetUserByUname(uname);
      StatusOfUserSearched.subscribe((result: any) => {
        if (result.length != 0) {
          this.UserData = result[0];
          console.log(this.UserData.uname);
        }
        else { }
      });
    });
  }
  UpdateUserData()
  {
    let StatusOfUpdate = this.DataService.UpdateUserData(this.UserData);
    StatusOfUpdate.subscribe((result:any)=>{
        if(result.affectedRows != 0)
        {
          this.router.navigate(['Profile']);
        }
        else
        {
          this.msg = "Something went wrong!";
        }
    });
  }
}
