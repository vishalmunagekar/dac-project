var express = require('express');
var cors = require('cors');
var SignupRouter = require('./signup/signup');
var LoginRouter = require('./login/login');
var ProfileRouter = require('./profile/profile');
var EditProfileRouter = require('./editprofile/editprofile');

var app = express();
let port = 4000;

app.use(cors());

// app.use(function(req, res, next) {
//   res.header("Access-Control-Allow-Origin", "*");
//   res.header('Access-Control-Allow-Methods', 'DELETE, PUT, GET, POST');
//   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//   next();
// });

app.use(express.json());

app.use("/signup", SignupRouter);
app.use("/login",LoginRouter);
app.use("/profile", ProfileRouter);
app.use("/editprofile", EditProfileRouter);

app.listen(port, function(){
  console.log("Server started on port No. " + port);
});
