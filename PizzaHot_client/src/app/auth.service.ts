import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { MD5, enc } from "crypto-js";

@Injectable({
  providedIn: 'root'
})
export class AuthService implements CanActivate {
  UserData: any
  constructor(public router: Router) { }

  isLoggedIn()
  {
    return (sessionStorage.getItem("isLoggedIn") == "1");
  }

  canActivate(route: ActivatedRouteSnapshot,
              state: RouterStateSnapshot)
  {
    if(this.isLoggedIn())
    {
      return true;
    }
    else
    {
      this.router.navigate(['Login']);
      return false;
    }
  }
  Login(credentials: { uname: any; pwd: any; } , result_data: { uname: any; pwd: any; })
  {
    console.log(credentials);
    console.log(result_data);
    const crypto_pwd = MD5(credentials.pwd);
    console.log(crypto_pwd);
    console.log(result_data.pwd);
    if (credentials.uname == result_data.uname &&
        crypto_pwd   == result_data.pwd)
    {
      sessionStorage.setItem("isLoggedIn", "1");
      this.UserData = result_data;
      return true;
    }
    else
    {
      return false;
    }
  }
  Logout()
  {
    sessionStorage.setItem("isLoggedIn", "0");
    this.router.navigate(['Login']);
  }

  UserProfileData()
  {
    return this.UserData;
  }
}
