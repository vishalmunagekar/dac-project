import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(public http: HttpClient) { }

  LoginData(uname: string) {
    return this.http.get("http://localhost:4000/login/" + uname);
  }
  SignupData(signup_data: any) {
    return this.http.post("http://localhost:4000/signup", signup_data);
  }
  UserProfileData(uname: string)
  {
    return this.http.get("http://localhost:4000/profile/" + uname);
  }
  GetUserByUname(uname: string)
  {
    return this.http.get("http://localhost:4000/editprofile/" + uname);
  }
  UpdateUserData(UserData)
  {
    return this.http.put("http://localhost:4000/editprofile/" + UserData.uname, UserData);
  }
}
